import "./App.css";
import React, { useState } from "react";

function App() {
  const [n, setN] = useState(0);
  const [listNumber, setListNumber] = useState([]);

  const getData = () => {
    const link = `https://8ixoc0qotl.execute-api.us-east-1.amazonaws.com/default/createRandomNumber?n=${n}`;

    if (isNaN(n) || n < 0 || n > 9) {
      setListNumber(["Insert number between 0 and 9"]);
      return;
    }

    setN(parseInt(n));

    console.log(`link = ${link}`);
    fetch(link)
      .then((res) => res.json())
      .then(
        (result) => {
          setListNumber(result);
        },
        // Nota: è importante gestire gli errori qui
        // invece di un blocco catch() in modo da non fare passare
        // eccezioni da bug reali nei componenti.
        (error) => {
          console.log(error);
        }
      );
  };

  return (
    <div className="App">
      <h2>How many values you want to generate (between 0 and 9):</h2> <br />
      <input
        type="text"
        value={n}
        onChange={(e) => {
          setN(e.target.value);
        }}
      />
      <br /> <br />
      <button onClick={getData}>Get numbers</button>
      <br /> <br />
      <h4>Random generated numbers: </h4>
      <br />
      <ul>
        {listNumber.map((number) => (
          <li>{number}</li>
        ))}
      </ul>
    </div>
  );
}

export default App;
